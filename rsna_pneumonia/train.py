from itertools import islice
import numpy as np
import os
import matplotlib.pyplot as plt

from sklearn.model_selection import KFold, train_test_split

from keras.models import load_model
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard
import keras.backend as K

from rsna_pneumonia.generators.img_generators import medical_imgs_generator
from rsna_pneumonia.utils.generic_utils import get_filenames

from rsna_pneumonia.model.unets import build_unet_model

from rsna_pneumonia.model.custom_losses import bce_dice_loss, focal_dice_loss
from rsna_pneumonia.model.custom_metrics import competitionMetric, mask_accuracy
from rsna_pneumonia.model.lovasz_hinge_loss import lovasz_loss
from rsna_pneumonia.model.custom_callbacks import CyclicLR
from rsna_pneumonia.utils.generic_utils import LoadParameters, get_model_and_preprocessor, get_pneumonia_locations

params = LoadParameters()
ROOT_DIR = params.params['root_fp']
TRAIN_IMAGE_DIR = params.params['train_image_fp']
WEIGHTS_DIR = params.params['weights_fp']
TENSORBOARD_DIR = params.params['tensorboard_fp']
ONLY_PNEUMONIA = params.params['only_pneumonia']
PNEUMONIA_DF = params.params['pneumonia_df']
LOSS = bce_dice_loss
#############################################################
BATCH_SIZE = params.params['batch_size']
LR = 0.0001  # 0.001 is default
EPOCHS = 150
MODEL_NAME = params.params['model_name']
_, PREPROCESSOR = get_model_and_preprocessor(MODEL_NAME)
NUM_FOLDS = 10
START_FOLD = 0
END_FOLD = 5
#############################################################


def train_single_fold(train_gen, valid_gen, model, model_fp, steps_per_epoch, validation_steps,
                      callbacks=(), loss=bce_dice_loss):
    model.compile(loss=loss, optimizer=Adam(lr=LR, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False),
                  metrics=[competitionMetric, mask_accuracy])

    checkpoint = ModelCheckpoint(model_fp, monitor='val_competitionMetric', mode='max', verbose=1,
                                 save_best_only=True, save_weights_only=False, period=1)
    callbacks = [checkpoint] + list(callbacks)

    history = model.fit_generator(train_gen,
                                  steps_per_epoch=steps_per_epoch,
                                  epochs=EPOCHS,
                                  verbose=1,
                                  callbacks=callbacks,
                                  validation_data=valid_gen,
                                  validation_steps=validation_steps,
                                  class_weight=None,
                                  max_queue_size=15,
                                  workers=3,
                                  use_multiprocessing=False,
                                  )


def train_kfold(image_filenames, num_folds, debug=False, callbacks=(), reload_weights=False,loss=bce_dice_loss, **kwargs):
    '''

    :param image_filenames: Filenames of the images on which to train on
    :param num_folds:
    :param debug: If set to True, images and masks will be shown from the generators
    :param callbacks: List of callbacks used in fit_generator
    :param kwargs: Augmentation methods, as supported by keras.preprocessing.image.ImageDataGenerator
    :return:
    '''
    k_folds = KFold(num_folds, random_state=42)

    folds = islice(k_folds.split(image_filenames), START_FOLD, END_FOLD)
    image_filenames = np.array(image_filenames, dtype=object)

    for fold, (train_index, valid_index) in enumerate(folds, start=START_FOLD):
        print('fold: {}'.format(fold))
        train_filenames = image_filenames[train_index]
        valid_filenames = image_filenames[valid_index]

        train_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, train_filenames, BATCH_SIZE,
                                           preprocess_input=PREPROCESSOR,
                                           **kwargs)

        valid_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, valid_filenames, BATCH_SIZE,
                                           preprocess_input=PREPROCESSOR,
                                           shuffle=False,
                                           )

        print('~~~~~~~~~')
        print(train_gen.n)
        print(valid_gen.n)
        #################################################
        if debug and fold == 0:
            imgs, masks = next(train_gen)
            for img, mask in zip(imgs, masks):
                plt.imshow(img[:, :, 1] - mask[:, :, 0])
                plt.show()
            imgs, masks = next(valid_gen)
            for img, mask in zip(imgs, masks):
                plt.imshow(img[:, :, 1] - mask[:, :, 0])
                plt.show()
                print('~~~~~~~~~~')
                print(np.max(mask))
                print(np.max(img))
                print(np.min(mask))
                print(np.min(img))
        #################################################

        model_fp = os.path.join(WEIGHTS_DIR, params.params['model_name'] + '_unet{}.hdf5'.format(fold))
        K.clear_session()

        if reload_weights:
            model = load_model(model_fp, custom_objects={'competitionMetric': competitionMetric,
                                                         'loss': bce_dice_loss,
                                                         'bce_dice_loss': bce_dice_loss,
                                                         'lovasz_loss': lovasz_loss,
                                                         'focal_dice_loss': focal_dice_loss})
        else:
            model = build_unet_model(MODEL_NAME)

        if fold == 0:
            model.summary()

        train_single_fold(train_gen, valid_gen,
                          model, model_fp,
                          steps_per_epoch=min(train_gen.n // BATCH_SIZE, 800),
                          validation_steps=valid_gen.n // BATCH_SIZE,
                          callbacks=list(callbacks),
                          loss=loss)


if __name__ == '__main__':
    image_filenames = get_filenames(image_folder=TRAIN_IMAGE_DIR)

    pneumonia_locations = get_pneumonia_locations()
    has_pneumonia = [1 if filename in pneumonia_locations else 0 for filename in image_filenames]
    image_filenames_for_stage_one, image_filenames_for_stage_two = train_test_split(image_filenames, test_size=0.1,
                                                                                    stratify=has_pneumonia,
                                                                                    random_state=0)
    if ONLY_PNEUMONIA:
        print('Only images with pneumonia will be considered in the image generator.')
        image_filenames_for_stage_one = [filename for filename in image_filenames_for_stage_one
                                         if filename.split('.')[0] in pneumonia_locations]

    callbacks = [EarlyStopping(monitor='val_competitionMetric', mode='max', patience=12, verbose=1),
                 ReduceLROnPlateau(monitor='val_competitionMetric', mode='max', factor=0.75, patience=3, min_lr=0.000075, verbose=1),
                 TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0, write_graph=True, write_images=True)
                 ]

    train_kfold(image_filenames_for_stage_one, num_folds=NUM_FOLDS,
                callbacks=callbacks, vertical_flip=True, loss=LOSS)
