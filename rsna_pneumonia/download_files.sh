#!/usr/bin/env bash
#run in assets folder

kaggle competitions download -c rsna-pneumonia-detection-challenge

!unzip -q -o stage_1_test_images.zip -d stage_1_test_images
!unzip -q -o stage_1_train_images.zip -d stage_1_train_images
!unzip -q -o stage_1_train_labels.csv.zip