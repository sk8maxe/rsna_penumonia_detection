def get_mobilenet_connecting_layers(backbone_model):
    act_128_64 = backbone_model.get_layer('conv_pw_1_relu').output

    act_64_128 = backbone_model.get_layer('conv_pw_3_bn').output

    act_32_256 = backbone_model.get_layer('conv_pw_5_relu').output

    act_16_512 = backbone_model.get_layer('conv_pw_11_relu').output

    act_8_1024 = backbone_model.get_layer('conv_pw_13_relu').output

    return [act_128_64, act_64_128, act_32_256, act_16_512, act_8_1024]

if __name__=='__main__':
    from keras.applications.mobilenet import MobileNet
    from rsna_pneumonia.model.unets import unet_v1, build_backbone_model

    backbone_model_ = build_backbone_model(MobileNet, pretrained_weights_fp=None)
    backbone_model_.summary()
    connecting_layers = get_mobilenet_connecting_layers(backbone_model_)
    model = unet_v1(backbone_model_, connecting_layers)
    model.summary()
