import keras.backend as K

import tensorflow as tf
import numpy as np

from rsna_pneumonia.utils.bbox_utils import unet_mask_to_bbox_coords


def castF(x):
    return K.cast(x, K.floatx())

def castB(x):
    return K.cast(x, bool)


def mask_accuracy(y_true, y_pred):
    y_true = K.batch_flatten(y_true)
    y_pred = K.batch_flatten(y_pred)

    y_pred_binarized = castF(K.greater(y_pred, 0.5))

    y_trueSum = K.sum(y_true, axis=-1)
    y_predSum = K.sum(y_pred_binarized, axis=-1)

    #has mask or not per image - (batch,)
    true_has_mask = castF(K.greater(y_trueSum, 1))
    pred_has_mask = castF(K.greater(y_predSum, 1))

    batch_size = K.sum(castF(K.greater(y_trueSum, -1)))

    truePositiveMask = K.sum(true_has_mask * pred_has_mask)
    truePositiveNoMask = K.sum((1 - true_has_mask) * (1- pred_has_mask))

    return (truePositiveMask + truePositiveNoMask) / batch_size


def competitionMetric(y_true, y_pred):
    '''
    Implementation of rsna pneumonia competition metric
    '''
    def np_competitionMetric2(np_true, np_pred):
        return np.mean([map_iou(unet_mask_to_bbox_coords(true), unet_mask_to_bbox_coords(pred))
                               for true, pred in zip(np_true, np_pred)]).astype(np.float32)

    return tf.py_func(np_competitionMetric2,
                      inp=[y_true, y_pred],
                      Tout=tf.float32,
                      stateful=False,
                      name='competitionMetric'
                      )


# helper function to calculate IoU
def iou_bbox(box1, box2):
    x11, y11, w1, h1 = box1
    x21, y21, w2, h2 = box2
    assert w1 * h1 > 0
    assert w2 * h2 > 0
    x12, y12 = x11 + w1, y11 + h1
    x22, y22 = x21 + w2, y21 + h2

    area1, area2 = w1 * h1, w2 * h2
    xi1, yi1, xi2, yi2 = max([x11, x21]), max([y11, y21]), min([x12, x22]), min([y12, y22])

    if xi2 <= xi1 or yi2 <= yi1:
        return 0
    else:
        intersect = (xi2-xi1) * (yi2-yi1)
        union = area1 + area2 - intersect
        return intersect / union


def map_iou(boxes_true, boxes_pred, thresholds=(0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75)):
    """
    Mean average precision at differnet intersection over union (IoU) threshold

    input:
        boxes_true: Mx4 numpy array of ground true bounding boxes of one image.
                    bbox format: (x1, y1, w, h)
        boxes_pred: Nx4 numpy array of predicted bounding boxes of one image.
                    bbox format: (x1, y1, w, h)
        thresholds: IoU shresholds to evaluate mean average precision on
    output:
        map: mean average precision of the image
    """

    # According to the introduction, images with no ground truth bboxes will not be
    # included in the map score unless there is a false positive detection (?)

    # return 0 if both are empty, don't count the image in final evaluation (?)
    if len(boxes_true) == 0 and len(boxes_pred) == 0:
        return 0

    map_total = 0

    # loop over thresholds
    for t in thresholds:
        matched_bt = set()
        tp, fn = 0, 0
        for i, bt in enumerate(boxes_true):
            matched = False
            for j, bp in enumerate(boxes_pred):
                miou = iou_bbox(bt, bp)
                if miou >= t and not matched and j not in matched_bt:
                    matched = True
                    tp += 1 # bt is matched for the first time, count as TP
                    matched_bt.add(j)
            if not matched:
                fn += 1 # bt has no match, count as FN

        fp = len(boxes_pred) - len(matched_bt) # FP is the bp that not matched to any bt
        m = tp / (tp + fn + fp)
        map_total += m

    return map_total / len(thresholds)


if __name__=="__main__":
    sess = K.get_session()

    y_true_array = np.zeros((24, 128, 128, 1))
    y_true_array[:, 20: 40, 20:40, :] = 1
    y_true = tf.Variable(y_true_array, dtype='float32', name='y_true')

    y_pred_array = np.zeros((24, 128, 128, 1))
    y_pred_array[5:, 20: 37, 20:37, :] = 1
    y_pred_array[:10, 100:115, 100:115, :] = 1
    y_pred = tf.Variable(y_pred_array, dtype='float32', name='y_pred')

    metric = competitionMetric(y_true, y_pred)
    sess.run(tf.global_variables_initializer())

    metric_ = sess.run(metric)
    print(metric_)

    ###########
    boxes_true = np.array([[20, 20, 20, 20]])
    boxes_pred = np.array([[20, 20, 17, 17], [100, 100, 15, 15]])
    bbox_metric = np.mean([map_iou(unet_mask_to_bbox_coords(true), unet_mask_to_bbox_coords(pred))
                           for true, pred in zip(y_true_array, y_pred_array)])
    print(bbox_metric)
