from keras.layers import Permute, Concatenate, Dense, Reshape, Input, multiply, RepeatVector
from keras.models import Model
import keras.backend as K


def concatenate_block(connecting_layer, word_vector): # ratio=16 default
    connecting_layer_dim = connecting_layer._keras_shape[2]

    # Repeat and reshape word vector from (embed_size,) to (connecting_layer_dim, connecting_layer_dim, embed_size)
    word_vector = RepeatVector(connecting_layer_dim)(word_vector)
    word_vector = Reshape((-1,))(word_vector)
    word_vector = RepeatVector(connecting_layer_dim)(word_vector)
    word_vector = Reshape((connecting_layer_dim, connecting_layer_dim, -1))(word_vector)

    if K.image_data_format() == 'channels_first':
        word_vector = Permute((3, 1, 2))(word_vector)

    x = Concatenate()([connecting_layer, word_vector])
    return x


def transfrom_connceting_layers(word_vector, connecting_layers, transform=(-1, -2, -3)):

    for i in transform:
        connecting_layer = connecting_layers[i]
        connecting_layer = concatenate_block(connecting_layer, word_vector)
        connecting_layers[i] = connecting_layer

    return connecting_layers


if __name__ == "__main__":
    from rsna_pneumonia.model.unets import unet_v1, build_backbone_model
    from rsna_pneumonia.model.unet_xception.unet_xception import get_xception_connecting_layers
    from keras.applications.xception import Xception

    backbone_model_ = build_backbone_model(Xception, pretrained_weights_fp=None)
    word_vector = Input((300,), name='word_vector_input')

    connecting_layers = get_xception_connecting_layers(backbone_model_)
    connecting_layers = transfrom_connceting_layers(word_vector, connecting_layers)
    backbone_model_ = Model(inputs=[word_vector, backbone_model_.input], outputs=[backbone_model_.output])

    model = unet_v1(backbone_model_, connecting_layers)
    model.summary()
