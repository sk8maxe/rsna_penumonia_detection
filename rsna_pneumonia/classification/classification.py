from sklearn.model_selection import train_test_split
import os

from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard

from keras.applications.xception import Xception
from keras.applications.xception import preprocess_input as xception_preprocess_input

from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input as vgg16_preprocess_input

from rsna_pneumonia.model.unets import build_backbone_model
from rsna_pneumonia.utils.generic_utils import get_filenames
from rsna_pneumonia.generators.img_generators import medical_imgs_generator
from rsna_pneumonia.utils.generic_utils import LoadParameters, get_model_and_preprocessor, get_pneumonia_locations

params = LoadParameters()
ROOT_DIR = params.params['root_fp']
TRAIN_IMAGE_DIR = params.params['train_image_fp']
CLASSIFICATION_MODEL_FILE = params.params['classification_model_file']
TENSORBOARD_DIR = params.params['tensorboard_fp']

#############################################################
BATCH_SIZE = 32
lr = 0.0005 #0.001 is default
test_size = 0.1
epochs = 200

MODEL_NAME = params.params['model_name']
MODEL, PREPROCESSOR = get_model_and_preprocessor(MODEL_NAME)
NUM_FOLDS = 10
START_FOLD = 0
END_FOLD = 5
#############################################################

backbone_model = build_backbone_model(MODEL, weights='imagenet', pretrained_weights_fp=None, upsample=False)
backbone_model.summary()
backbone_model.compile(loss='binary_crossentropy', optimizer="adam", metrics=['mse', 'accuracy'])

early_stopping = EarlyStopping(patience=10, verbose=1)
reduce_lr = ReduceLROnPlateau(factor=0.75, patience=3, min_lr=0.000005, verbose=1)
checkpoint = ModelCheckpoint(CLASSIFICATION_MODEL_FILE, monitor='val_loss', verbose=1,
                             save_best_only=True, save_weights_only=False, mode='min', period=1)
tb = TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0,
                 write_graph=True, write_images=True)
#############################################################

image_filenames = get_filenames(image_folder=TRAIN_IMAGE_DIR)
pneumonia_locations = get_pneumonia_locations()
has_pneumonia = [1 if filename in pneumonia_locations else 0 for filename in image_filenames]

image_filenames, image_filenames_for_stage_two = train_test_split(image_filenames, test_size=0.1,
                                                                  stratify=has_pneumonia, random_state=0)

train_filenames, valid_filenames = train_test_split(image_filenames, test_size=0.1,
                                                    stratify=has_pneumonia, random_state=0)

train_gen = medical_imgs_generator(TRAIN_IMAGE_DIR,
                                   train_filenames,
                                   BATCH_SIZE,
                                   class_mode='categorical',
                                   preprocess_input=PREPROCESSOR,
                                   shuffle=True)

valid_gen = medical_imgs_generator(TRAIN_IMAGE_DIR,
                                   valid_filenames,
                                   BATCH_SIZE,
                                   class_mode='categorical',
                                   preprocess_input=PREPROCESSOR,
                                   shuffle=False,
                                   )
#############################################################

history = backbone_model.fit_generator(train_gen,
                                       steps_per_epoch=train_gen.n // BATCH_SIZE,
                                       epochs=epochs,
                                       verbose=1,
                                       callbacks=[early_stopping, reduce_lr, checkpoint, tb],
                                       validation_data=valid_gen,
                                       validation_steps=valid_gen.n // BATCH_SIZE,
                                       class_weight=None,
                                       max_queue_size=10,
                                       workers=1,
                                       use_multiprocessing=False,
                                       shuffle=True,
                                       initial_epoch=0)
