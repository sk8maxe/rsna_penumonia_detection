from sklearn.model_selection import train_test_split
import os

from keras.layers import Input, Concatenate, Dense, Dropout, Activation
from keras.models import Model
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint, TensorBoard
from rsna_pneumonia.utils.generic_utils import get_filenames
from rsna_pneumonia.generators.img_generators import medical_imgs_generator
from rsna_pneumonia.utils.generic_utils import LoadParameters, get_model_and_preprocessor, get_pneumonia_locations

params = LoadParameters()
ROOT_DIR = params.params['root_fp']
TRAIN_IMAGE_DIR = params.params['train_image_fp']
CLASSIFICATION_MODEL_FILE = params.params['classification_model_file']
TENSORBOARD_DIR = params.params['tensorboard_fp']

#############################################################
BATCH_SIZE = 48
lr = 0.0005 #0.001 is default
test_size = 0.1
epochs = 200

MODEL_NAME = params.params['model_name']
MODEL, PREPROCESSOR = get_model_and_preprocessor(MODEL_NAME)
NUM_FOLDS = 10
START_FOLD = 0
END_FOLD = 5
#############################################################

def categorical_model():
    ''' Return value of load_img_metadata in img_generator:
                [np.log1p(dcm_data.PatientAge),
                float(dcm_data.PatientSex == 'F'),
                float(dcm_data.PixelSpacing[0]),
                float(dcm_data.ViewPosition == 'PA'),
                ]
    :return:
    '''
    age_input = Input((1,), name='age_input')
    patient_sex_input = Input((1,), name='patient_sex')
    pixel_spacing_input = Input((1,), name='pixel_spacing')
    view_position_input = Input((1,), name='view_position')

    inp_concatenated = Concatenate()([age_input, patient_sex_input, pixel_spacing_input, view_position_input])
    hidden_1 = Dense(12, activation='relu')(inp_concatenated)
    hidden_2 = Dense(8, activation='relu')(hidden_1)
    out = Dense(1, activation='sigmoid')(hidden_2)

    model = Model(inputs=[age_input, patient_sex_input, pixel_spacing_input, view_position_input], outputs=[out])
    return model


def train_classifier(model, class_mode='metadata_classification'):
    early_stopping = EarlyStopping(patience=10, verbose=1)
    reduce_lr = ReduceLROnPlateau(factor=0.75, patience=3, min_lr=0.000005, verbose=1)
    checkpoint = ModelCheckpoint(CLASSIFICATION_MODEL_FILE, monitor='val_loss', verbose=1,
                                 save_best_only=True, save_weights_only=False, mode='min', period=1)
    tb = TensorBoard(log_dir=TENSORBOARD_DIR, histogram_freq=0,
                     write_graph=True, write_images=True)

    #############################################################
    image_filenames = get_filenames(image_folder=TRAIN_IMAGE_DIR)
    pneumonia_locations = get_pneumonia_locations()
    has_pneumonia = [1 if filename in pneumonia_locations else 0 for filename in image_filenames]

    image_filenames, image_filenames_for_stage_two = train_test_split(image_filenames, test_size=0.1,
                                                                      stratify=has_pneumonia, random_state=0)

    train_filenames, valid_filenames = train_test_split(image_filenames, test_size=0.1, random_state=0)

    train_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, train_filenames, BATCH_SIZE,
                                       class_mode=class_mode,
                                       preprocess_input=PREPROCESSOR)
    valid_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, valid_filenames, BATCH_SIZE,
                                       class_mode=class_mode,
                                       preprocess_input=PREPROCESSOR,
                                       shuffle=False)
    #############################################################

    history = model.fit_generator(train_gen,
                                           steps_per_epoch=train_gen.n // BATCH_SIZE,
                                           epochs=epochs,
                                           verbose=1,
                                           callbacks=[early_stopping, reduce_lr, checkpoint, tb],
                                           validation_data=valid_gen,
                                           validation_steps=valid_gen.n // BATCH_SIZE,
                                           class_weight=None,
                                           max_queue_size=10,
                                           workers=1,
                                           use_multiprocessing=False,
                                           shuffle=True,
                                           initial_epoch=0)
    return history


if __name__=='__main__':
    model = categorical_model()
    model.compile(loss='mse',
                  optimizer='Adam',
                  metrics=['accuracy'])

    model.summary()
    train_classifier(model)
