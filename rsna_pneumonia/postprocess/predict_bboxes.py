import logging
import os
from itertools import islice

import keras.backend as K
import numpy as np
import pandas as pd
from keras.applications.xception import preprocess_input as xception_preprocess_input
from keras.models import load_model

from rsna_pneumonia.model.custom_losses import bce_dice_loss
from rsna_pneumonia.model.custom_metrics import competitionMetric, mask_accuracy
from rsna_pneumonia.generators.img_generators import medical_imgs_generator
from rsna_pneumonia.utils.bbox_utils import unet_mask_to_bbox_coords
from rsna_pneumonia.utils.generic_utils import LoadParameters, divisorGenerator

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

params = LoadParameters()
img_size = params.params['img_size']
IMAGE_SHAPE = (img_size, img_size)


REMOVE_THRESHOLD = params.params['remove_threshold']
THRESHOLD = params.params['threshold']

ROOT_DIR = params.params['root_fp']
TEST_IMAGE_DIR = params.params['test_image_fp']

PREPROCESSOR = xception_preprocess_input


def masks_to_submission_dict(masks, patientIds):
    '''
    Transforms the predicted masks (as images with pixel range (0-1)) to bbox coordinates and creates the submission
    dictionary.
    :param masks:
    :param patientIds:
    :return:
    '''
    # create submission dictionary
    submission_dict = {}
    # loop through testset
    for mask, patiendId in zip(masks, patientIds):
        # divide predicted mask by 255 to be in the range (0,1)
        mask = mask.astype(np.float32) / 255
        bboxes = unet_mask_to_bbox_coords(mask, threshold=0.5, do_resize=True)

        prediction_string = ''
        for (x, y, height, width) in bboxes:
            x, y, height, width = int(x), int(y), int(height), int(width)
            # add to prediction_string, first value is confidence which is not important for prediction
            prediction_string += str(0.5) + ' ' + str(x) + ' ' + str(y) + ' ' + str(width) + ' ' + str(height) + ' '
        # add patiendId and prediction_string to dictionary
        submission_dict[patiendId] = prediction_string

    return submission_dict


def remove_at(image, threshold):
  pixels = np.sum(image[image > THRESHOLD].astype(bool)) / (image.shape[0] * image.shape[1])
  if pixels <= threshold:
    return np.zeros_like(image)
  else:
    return image


def chunks(slicable, num_chunks, chunk_size):
    """Yield successive n-sized chunks from l."""
    for i in range(0, num_chunks, chunk_size):
        yield next(islice(slicable, i, i + chunk_size))


def flipup_fliplr(x):
    return np.flipud(np.fliplr(x))


def tta_predictions(filenames, model,
                    remove_empty_masks=False, threshold=0.,
                    tta_methods=(np.fliplr, np.flipud, flipup_fliplr)):

    predictions = np.array([]).reshape((0,) + IMAGE_SHAPE + (1,))

    batch_size = max([divisor for divisor in divisorGenerator(len(filenames)) if divisor < 64])

    image_gen = medical_imgs_generator(TEST_IMAGE_DIR,
                                       filenames,
                                       batch_size=batch_size,
                                       class_mode='image',
                                       preprocess_input=PREPROCESSOR,
                                       shuffle=False)

    num_chunks = len(filenames) // batch_size

    for _ in range(num_chunks):
        images = next(image_gen)
        batch_preds = model.predict(images, verbose=0)
        for method in tta_methods:
            transformed_chunk = np.array([method(image) for image in images])
            preds_transformed = model.predict(transformed_chunk, verbose=1)
            untransformed_preds = np.array([method(mask) for mask in preds_transformed])

            batch_preds = batch_preds + untransformed_preds

        batch_preds = batch_preds / (len(tta_methods) + 1)
        if remove_empty_masks:
            batch_preds = np.array([remove_at(mask, threshold) for mask in batch_preds])

        predictions = np.concatenate([predictions, batch_preds])
    return predictions


def predict_kfold(filenames, num_folds, tta_methods):
    '''
    Returns integer predictions in range 0-255
    :param num_folds:
    :return:
    '''
    predictions = np.zeros((len(filenames),) + IMAGE_SHAPE + (1,), dtype='int32')

    for fold in range(num_folds):
        logging.info('Predict on fold {}'.format(fold))
        model_fp = os.path.join(params.params['weights_fp'], params.params['model_name'] + '_unet{}.hdf5'.format(fold))
        K.clear_session()
        model = load_model(model_fp, custom_objects={'mean_imagenet': np.array([0.5, 0.5, 0.5]),
                                                       'bce_dice_loss': bce_dice_loss,
                                                      'competitionMetric': competitionMetric,
                                                       'focal_dice_loss': bce_dice_loss,
                                                     'mask_accuracy': mask_accuracy})

        fold_predictions = tta_predictions(filenames, model,
                                           remove_empty_masks=False, threshold=0.,
                                           tta_methods=tta_methods)

        fold_predictions = (255 * fold_predictions).astype(np.int32)

        predictions += fold_predictions

    return predictions // num_folds


if __name__ == '__main__':
    num_folds = 1
    test_filenames = os.listdir(TEST_IMAGE_DIR)
    patientIds = [filename.split('.')[0] for filename in test_filenames]

    print('n test samples:', len(test_filenames))

    # predictions are predicted mask images as dtype np.int32, ranging from 0-255
    predictions = predict_kfold(test_filenames, num_folds, tta_methods=(np.fliplr,))
    #np.save(r'C:\Users\game\Desktop\kaggle\rsna\predictions\5fold_xception_unetv4.npy', predictions)

    filename = os.path.join(ROOT_DIR, 'submission.csv')
    submission_dict = masks_to_submission_dict(predictions, patientIds)
    sub = pd.DataFrame.from_dict(submission_dict, orient='index')
    sub.index.names = ['patientId']
    sub.columns = ['PredictionString']
    sub.to_csv(filename)
