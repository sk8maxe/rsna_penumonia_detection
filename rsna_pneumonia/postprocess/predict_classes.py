import os
import numpy as np
import pandas as pd

from keras.models import load_model

from rsna_pneumonia.generators.img_generators import medical_imgs_generator
from rsna_pneumonia.utils.generic_utils import get_filenames
from rsna_pneumonia.utils.generic_utils import LoadParameters, get_model_and_preprocessor, divisorGenerator

params = LoadParameters()
img_size = params.params['img_size']
IMAGE_SHAPE = (img_size, img_size)


REMOVE_THRESHOLD = params.params['remove_threshold']
THRESHOLD_CLASSIFICATION = params.params['threshold_classification']
ROOT_DIR = params.params['root_fp']
TEST_IMAGE_DIR = params.params['test_image_fp']
CSV_FILENAME = os.path.join(ROOT_DIR, 'submission.csv')
BATCH_SIZE = 25


_, PREPROCESSOR = get_model_and_preprocessor(params.params['model_name'])
model_fp = r'C:\Users\game\Desktop\kaggle\rsna\xception_classifiaction.hdf5'


if __name__ == '__main__':
    predictions_df = pd.read_csv(CSV_FILENAME, index_col='patientId')

    test_filenames = get_filenames(image_folder=TEST_IMAGE_DIR)
    patientIds = [filename.split('.')[0] for filename in test_filenames]

    model = load_model(model_fp)

    batch_size = max([divisor for divisor in divisorGenerator(len(test_filenames)) if divisor < 64])
    test_gen = medical_imgs_generator(TEST_IMAGE_DIR,
                                      test_filenames,
                                      batch_size,
                                      class_mode='image',
                                      preprocess_input=PREPROCESSOR,
                                      shuffle=False)

    class_predictions_ = model.predict_generator(test_gen,
                                                steps=len(test_filenames) // batch_size,
                                                max_queue_size=10,
                                                workers=1,
                                                use_multiprocessing=False,
                                                verbose=1)

    penumonia_predictions = class_predictions_ > THRESHOLD_CLASSIFICATION

    for patientId, penumonia_prediction in zip(patientIds, penumonia_predictions):
        if not penumonia_prediction:
            predictions_df.loc[patientId]['PredictionString'] = ''

    print(predictions_df.describe())
    predictions_df.to_csv(r'C:\Users\game\Desktop\kaggle\rsna\mod_submission.csv', index=True)
