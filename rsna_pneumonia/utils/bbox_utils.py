import numpy as np
from skimage import measure
from skimage.transform import resize


def unet_mask_to_bbox_coords(mask, threshold=0.5, do_resize=False):
    '''
    :param mask: predicted mask, numpy array of shape (widht, height, 1) or (width, height)
    :param threshold: threshold for binarization of mask
    :return: bbox coordinates, in form [x, y, width, height] for each bbox coordinate
    :rtype: numy array
    '''
    if mask.ndim == 3:
        mask = mask[:, :, 0]
    # resize predicted mask
    if do_resize:
        mask = resize(mask, (1024, 1024), mode='constant')
    # threshold predicted mask, multiply by 255, since predictions were upscaled for memory performance
    comp = mask > threshold
    # apply connected components
    comp = measure.label(comp)

    bboxes = np.array([]).reshape((0, 4))

    for region in measure.regionprops(comp):
        # retrieve x, y, height and width
        y, x, y2, x2 = region.bbox
        height = y2 - y
        width = x2 - x
        bboxes = np.concatenate([bboxes, np.array([[x, y, width, height]])], axis=0)

    return bboxes


def bboxes_to_img(bboxes, img_shape=(1024, 1024)):

    mask = np.zeros(img_shape)
    for location in bboxes:
        # add 1's at the location of the pneumonia
        x, y, width, height = location
        x, y, width, height = int(x), int(y), int(width), int(height)
        mask[y: y + height, x: x + width] = 1

    return mask


def unet_mask_to_bboxes(mask):
    mask_as_bboxes = np.zeros_like(mask)
    # get filename without extension
    bboxes = unet_mask_to_bbox_coords(mask, threshold=0.5)
    # loop through pneumonia
    for location in bboxes:
        # add 1's at the location of the pneumonia
        x, y, width, height = location
        x, y, width, height = int(x), int(y), int(width), int(height)
        mask_as_bboxes[y: y + height, x: x + width] = 1

    return mask_as_bboxes


if __name__=='__main__':
    mask = np.zeros((128, 128,  1))
    mask[20:40, 20: 40] = 0.55
    bbox = unet_mask_to_bbox_coords(mask, threshold=0.5, do_resize=True)
    print(bbox)
    print(20 * 1024/128)
