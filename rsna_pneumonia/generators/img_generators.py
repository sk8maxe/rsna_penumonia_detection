import numpy as np
import os
import matplotlib.pyplot as plt
import pydicom

from sklearn.model_selection import train_test_split
from keras.preprocessing import image
from skimage.transform import resize
import keras.backend as K
from keras.models import load_model

from rsna_pneumonia.utils.generic_utils import get_pneumonia_locations, get_filenames
from rsna_pneumonia.model.custom_losses import bce_dice_loss
from rsna_pneumonia.model.custom_metrics import competitionMetric, mask_accuracy
from rsna_pneumonia.utils.bbox_utils import unet_mask_to_bboxes
from rsna_pneumonia.utils.generic_utils import LoadParameters, transform_list

params = LoadParameters()
img_size = params.params['img_size']
IMAGE_SHAPE = (img_size, img_size)
ONLY_PNEUMONIA = params.params['only_pneumonia']
ROOT_DIR = params.params['root_fp']
TRAIN_IMAGE_DIR = params.params['train_image_fp']


class ImageDataGeneratorDcm(image.ImageDataGenerator):

    def flow_from_directory(self, directory,
                            preprocess_input=lambda x: x,
                            target_size=IMAGE_SHAPE, color_mode='rgb',
                            classes=None, class_mode='categorical',
                            batch_size=32, shuffle=True, seed=None,
                            save_to_dir=None,
                            save_prefix='',
                            save_format='png',
                            follow_links=False,
                            subset=None,
                            interpolation='nearest'):
        return DirectoryIteratorDcm(
            directory, self,
            preprocess_input=preprocess_input,
            target_size=target_size, color_mode=color_mode,
            classes=classes, class_mode=class_mode,
            data_format=self.data_format,
            batch_size=batch_size, shuffle=shuffle, seed=seed,
            save_to_dir=save_to_dir,
            save_prefix=save_prefix,
            save_format=save_format,
            follow_links=follow_links,
            subset=subset,
            interpolation=interpolation)


class DirectoryIteratorDcm(image.DirectoryIterator):

    def __init__(self, *args, **kwargs):
        
        # grab pnedumonia locations, if they are needed for predictions
        if kwargs['class_mode'] not in ['image']:
            self.pneumonia_locations = get_pneumonia_locations()
        else:
            self.pneumonia_locations = {}

        self.preprocess_input = kwargs['preprocess_input']
        kwargs.pop('preprocess_input')

        # Grab class mode, and set class mode which is passed to the image.DirectoryIterator to categorical.
        # Afterwards set self.class_mode to the passed argument. This is needed, since the initializer of
        # image.DirectoryIterator raises an exception if he class mode is not in the default set of modes.
        # By this hack, we can circumvent the throwing of exception and use custom class modes.
        self.temp_class_mode = kwargs['class_mode']
        kwargs['class_mode'] = 'categorical'
        super(DirectoryIteratorDcm, self).__init__( *args, **kwargs)
        self.class_mode = self.temp_class_mode

    def load_dcm_img(self, filename):
        # load dicom file as numpy array
        img = pydicom.dcmread(os.path.join(self.directory, filename)).pixel_array
        # rescale image
        img = resize(img, (self.image_shape[0], self.image_shape[1]), mode='reflect')
        img = np.expand_dims(img, axis=2)
        img = img * 255
        return img

    def create_mask(self, filename):
        # create empty mask
        mask = np.zeros((self.image_shape[0], self.image_shape[1]) + (1,))
        # get filename without extension
        patientId = filename.split('.')[0]
        # if image contains pneumonia
        if patientId in self.pneumonia_locations.keys():
            # loop through pneumonia
            for location in self.pneumonia_locations[patientId]:
                # add 1's at the location of the pneumonia
                location = np.array(location) * self.image_shape[0] // 1024
                x, y, w, h = location
                mask[y:y + h, x:x + w] = 1
        return mask

    def load_img_metadata(self, filename):
        dcm_data = pydicom.dcmread(os.path.join(self.directory, filename))
        return [np.log1p(int(dcm_data.PatientAge)),
                float(dcm_data.PatientSex == 'F'),
                float(dcm_data.PixelSpacing[0]),
                #np.mean(dcm_data.pixel_array == 0),
                float(dcm_data.ViewPosition == 'PA'),
                ]

    def load_image_batch(self, index_array, seed=0):
        image_batch = np.zeros((len(index_array),) + self.image_shape, dtype=K.floatx())
        for i, j in enumerate(index_array):
            # get image!
            filename = self.filenames[j]
            img = self.load_dcm_img(filename)
            img = self.image_data_generator.random_transform(img, seed=seed)
            seed += 1
            image_batch[i] = img

        image_batch = np.concatenate([image_batch, image_batch, image_batch], axis=3)
        image_batch = self.preprocess_input(image_batch)

        return image_batch

    def load_mask_batch(self, index_array, seed=0):
        mask_batch = np.zeros((len(index_array),) + self.image_shape, dtype=K.floatx())
        for i, j in enumerate(index_array):
            # get image!
            filename = self.filenames[j]
            mask = self.create_mask(filename)
            mask = self.image_data_generator.random_transform(mask, seed=seed)
            seed += 1
            mask_batch[i] = mask

        return mask_batch

    def _get_batches_of_transformed_samples(self, index_array):
        seed = np.random.randint(10000)
        image_batch = self.load_image_batch(index_array, seed=seed)

        if self.class_mode is 'image':
            # Return image only, for predictions
            return image_batch

        mask_batch = self.load_mask_batch(index_array, seed=seed)

        if self.class_mode is 'categorical':
            # Return image and whether image has bboxes (i.e. pneumonia)
            classes = (np.sum(mask_batch, axis=tuple(range(1, mask_batch.ndim))) > 0).astype(np.int32)
            return image_batch, classes

        elif self.class_mode == 'metadata_classification':
            # return image and metadata, corresponding to the image
            img_metadatas = transform_list([self.load_img_metadata(self.filenames[j]) for j in index_array])
            classes = (np.sum(mask_batch, axis=tuple(range(1, mask_batch.ndim))) > 0).astype(np.int32)
            return img_metadatas, classes

        elif self.class_mode == 'img_metadata_classification':
            # return image and metadata, corresponding to the image
            img_metadatas = transform_list([self.load_img_metadata(self.filenames[j]) for j in index_array])
            classes = (np.sum(mask_batch, axis=tuple(range(1, mask_batch.ndim))) > 0).astype(np.int32)
            return [image_batch, img_metadatas], classes

        elif self.class_mode == 'unet':
            return image_batch, mask_batch

        else:
            raise NotImplementedError


def _transform_flow_generator(flow_gen, filenames):
    num_files = len(filenames)
    flow_gen.filenames = filenames
    flow_gen.classes = np.zeros(num_files)
    flow_gen.samples = num_files
    flow_gen.n = num_files


def medical_imgs_generator(image_dir, filenames, batch_size,
                           preprocess_input,
                           class_mode='unet',
                           shuffle=True,
                           **kwargs):

    image_datagen = ImageDataGeneratorDcm(batch_size, filenames, **kwargs)
    gen = image_datagen.flow_from_directory(image_dir,
                                            preprocess_input=preprocess_input,
                                            target_size=IMAGE_SHAPE,
                                            batch_size=batch_size,
                                            class_mode=class_mode,
                                            color_mode='grayscale',
                                            shuffle=shuffle)

    _transform_flow_generator(gen, filenames)
    return gen


def visualize_preds(batch_size, model, preprocess_input):

    image_filenames = get_filenames(image_folder=TRAIN_IMAGE_DIR)
    image_filenames, image_filenames_for_stage_two = train_test_split(image_filenames, test_size=0.1, random_state=0)
    train_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, image_filenames_for_stage_two, batch_size,
                                       class_mode='unet',
                                       preprocess_input=preprocess_input)

    imgs, masks = next(train_gen)
    pred_masks = model.predict(imgs)

    pred_masks_as_bboxes = np.zeros_like(pred_masks)
    for i, pred_mask in enumerate(pred_masks):
        mask_as_bbox = unet_mask_to_bboxes(pred_mask)
        pred_masks_as_bboxes[i] = mask_as_bbox

    for img, mask, pred_mask, pred_mask_as_bbox in zip(imgs, masks, pred_masks, pred_masks_as_bboxes):
        plt.imshow(img[:, :, 1])
        plt.show()
        plt.imshow(mask[:, :, 0])
        plt.show()
        plt.imshow(pred_mask[:, :, 0])
        plt.show()
        plt.imshow(pred_mask_as_bbox[:, :, 0])
        plt.show()
        plt.imshow(img[:, :, 1] - pred_mask[:, :, 0] + (mask[:, :, 0]))
        plt.show()


if __name__ == '__main__':

    from keras.applications.xception import preprocess_input

    batch_size = 8
    image_filenames = get_filenames(image_folder=TRAIN_IMAGE_DIR)
    train_gen = medical_imgs_generator(TRAIN_IMAGE_DIR, image_filenames, batch_size,
                                       class_mode='unet',
                                       preprocess_input=lambda x: x)
    print(train_gen.n)

    imgs, masks = next(train_gen)
    for img, mask in zip(imgs, masks):
        #plt.imshow(img[:, :, 1])
        #plt.show()
        #plt.imshow(mask[:, :, 0])
        #plt.show()
        plt.imshow(img[:, :, 1] - 255 * mask[:, :, 0])
        plt.show()

    model_fp = os.path.join(ROOT_DIR, 'weights/xception_unet{}.hdf5'.format(0))
    model = load_model(model_fp, custom_objects={'competitionMetric': competitionMetric,
                                                 'loss': bce_dice_loss,
                                                 'bce_dice_loss': bce_dice_loss,
                                                 'focal_dice_loss': bce_dice_loss,
                                                 'mask_accuracy': mask_accuracy})

    visualize_preds(batch_size=10, model=model,
                    preprocess_input=preprocess_input)
