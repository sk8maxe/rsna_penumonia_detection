from setuptools import setup
from setuptools import find_packages

long_description = '''unet models for rsna_pneumonia_detection'''

setup(name='rsna_pneumonia_detection',
      version='1.0.0',
      description='Keras based Segmentation Model',
      long_description=long_description,
      author='Max Jeblick',
      author_email='NA',
      url='https://gitlab.com/sk8maxe/tgs_salt',
      download_url='https://gitlab.com/sk8maxe/tgs_salt',
      license='MIT',
      install_requires=[],
      extras_require={
      },
      classifiers=[
      ],
      packages=find_packages())
